class Nodo{
    constructor(padre,Hijod,Hijoi,Pos,Nivel,valor,Nombre)
    {
        this.Padre = padre;
        this.HijoD=Hijod;
        this.HijoI=Hijoi;
        this.posicion=Pos;
        this.nivel=Nivel;
        this.Valor=valor;
        this.Nombre=Nombre
    }
}

function MostrarArbol() {
    var Raiz = new Nodo (null,null,null,"Padre",0,36,'36');

    Raiz.HijoI = new Nodo (Raiz,null,null,"Hijo Izquierdo",1,16,'16');
    Raiz.HijoD = new Nodo (Raiz,null,null,"Hijo Derecho",1,20,'20');

    Raiz.HijoI.HijoI = new Nodo (Raiz.HijoI,null,null,"Hijo Izquierdo",2,8,'8');
    Raiz.HijoI.HijoD = new Nodo (Raiz.HijoI,null,null,"Hijo Derecho",2,8,'8');

    Raiz.HijoD.HijoI = new Nodo (Raiz.HijoD,null,null,"Hijo Izquierdo",2,8,'8');
    Raiz.HijoD.HijoD = new Nodo (Raiz.HijoD,null,null,"Hijo Derecho",2,12,'12');

    Raiz.HijoI.HijoI.HijoI = new Nodo (Raiz.HijoI.HijoI,null,null,"Hijo Izquierdo",3,4,'e');
    Raiz.HijoI.HijoI.HijoD = new Nodo (Raiz.HijoI.HijoD,null,null,"Hijo Derecho",3,4,'4');

    Raiz.HijoI.HijoD.HijoI = new Nodo (Raiz.HijoI.HijoD,null,null,"Hijo Izquierdo",3,4,'a');
    Raiz.HijoI.HijoD.HijoD = new Nodo (Raiz.HijoI.HijoD,null,null,"Hijo Derecho",3,4,'4');

    Raiz.HijoD.HijoI.HijoI = new Nodo (Raiz.HijoD.HijoI,null,null,"Hijo Izquierdo",3,4,'4');
    Raiz.HijoD.HijoI.HijoD = new Nodo (Raiz.HijoD.HijoI,null,null,"Hijo Derecho",3,4,'4');

    Raiz.HijoD.HijoD.HijoI = new Nodo (Raiz.HijoD.HijoD,null,null,"Hijo Izquierdo",3,5,'5');
    Raiz.HijoD.HijoD.HijoD = new Nodo (Raiz.HijoD.HijoD,null,null,"Hijo Derecho",3,7,'');

    Raiz.HijoI.HijoI.HijoD.HijoI= new Nodo (Raiz.HijoI.HijoI.HijoD,null,null,"Hijo Izquierdo",4,2,'n');
    Raiz.HijoI.HijoI.HijoD.HijoD= new Nodo (Raiz.HijoI.HijoI.HijoD,null,null,"Hijo Derecho",4,2,'2');

    Raiz.HijoI.HijoD.HijoD.HijoI = new Nodo (Raiz.HijoI.HijoD.HijoD,null,null,"Hijo Izquierdo",4,2,'t');
    Raiz.HijoI.HijoD.HijoD.HijoD = new Nodo (Raiz.HijoI.HijoD.HijoD,null,null,"Hijo Derecho",4,2,'m');

    Raiz.HijoD.HijoI.HijoI.HijoI = new Nodo (Raiz.HijoD.HijoI.HijoI,null,null,"Hijo Izquierdo",4,2,'i');
    Raiz.HijoD.HijoI.HijoI.HijoD = new Nodo (Raiz.HijoD.HijoI.HijoI,null,null,"Hijo Derecho",4,2,'2');

    Raiz.HijoD.HijoI.HijoD.HijoI = new Nodo (Raiz.HijoD.HijoI.HijoD,null,null,"Hijo Izquierdo",4,2,'h');
    Raiz.HijoD.HijoI.HijoD.HijoD = new Nodo (Raiz.HijoD.HijoI.HijoD,null,null,"Hijo Derecho",4,2,'s');

    Raiz.HijoD.HijoD.HijoI.HijoI = new Nodo (Raiz.HijoD.HijoD.HijoI,null,null,"Hijo Izquierdo",4,2,'2');
    Raiz.HijoD.HijoD.HijoI.HijoD = new Nodo (Raiz.HijoD.HijoD.HijoI,null,null,"Hijo Derecho",4,3,'f');

    Raiz.HijoI.HijoI.HijoD.HijoD.HijoI = new Nodo (Raiz.HijoI.HijoI.HijoD.HijoD,null,null,"Hijo Izquierdo",5,1,'o');
    Raiz.HijoI.HijoI.HijoD.HijoD.HijoD = new Nodo (Raiz.HijoI.HijoI.HijoD.HijoD,null,null,"Hijo Derecho",5,1,'u');

    Raiz.HijoD.HijoI.HijoI.HijoD.HijoI = new Nodo (Raiz.HijoD.HijoI.HijoI.HijoD,null,null,"Hijo Izquierdo",5,1,'x');
    Raiz.HijoD.HijoI.HijoI.HijoD.HijoD = new Nodo (Raiz.HijoD.HijoI.HijoI.HijoD,null,null,"Hijo Derecho",5,1,'p');

    Raiz.HijoD.HijoD.HijoI.HijoI.HijoI = new Nodo (Raiz.HijoD.HijoD.HijoI.HijoI,null,null,"Hijo Izquierdo",5,1,'r');
    Raiz.HijoD.HijoD.HijoI.HijoI.HijoD = new Nodo (Raiz.HijoD.HijoD.HijoI.HijoI,null,null,"Hijo Derecho",5,1,'l');

    console.log(Raiz);
    return Raiz
}

var Resultado=[];
function BuscarAbol(Nodo,Valor){

  
    if(Nodo.Valor==Valor)
       {
        Resultado.push(Nodo);
       }
           if(Nodo.HijoI!=null)
           {
            BuscarAbol(Nodo.HijoI,Valor);
           }
           if(Nodo.HijoD!=null)
           {
            BuscarAbol(Nodo.HijoD,Valor);
           }

}

function  ShowResultado()
{
    console.log(Resultado);
}

var suma=0;

function SumaCamnio(Nodo,valor){

    if(valor!=undefined)
    {
        if(Nodo.Padre!=null)
        {
          suma+=Nodo.Padre.Valor;
          SumaCamnio(Nodo.Padre,valor);
        }
        
        return suma+valor;
    }
    else {
        return suma;
    }

}

var camino="";
function MostrandoCamnio(Nodo,valor){

    if(Nodo.Padre!=null)
    {
        camino += ''+ Nodo.Padre.Valor +'-';
        MostrandoCamnio(Nodo.Padre,valor);
    }
    
    return valor+"-"+camino;

}


var Original =[];
var Izquierdos=[];
var Derechos=[];
var Abuelo = null;

function  OrdenarArreglo()
{
    Original= $("#AbNode").val().split(",");

   for (let index = 1; index < Original.length; index++) {
     
       if(Original[0]>Original[index])
       {
           Izquierdos.push(Original[index]);
       }else {
            Derechos.push(Original[index]);
       }
       
   }
  Abuelo = new Nodo (null,null,null,"Padre",0,Original[0],Original[0]);
  CrearArbol(Abuelo);
  CrearIzquierdo(Abuelo);
}

function CrearArbol(Node)
{
    
         for (let index = 0; index < Derechos.length; index++) {
             if(Node.HijoD==null)
              {
                if(Node.Valor<Derechos[index])
                {
                    Node.HijoD=  new Nodo(Node,null,null,"Hijo Derecho",0,Derechos[index],Derechos[index]);
                    Derechos.splice(0,1);
                    CrearArbol(Node.HijoD);
                }else {
                    Node.HijoI=  new Nodo(Node,null,null,"Hijo Izquierdo",0,Derechos[index],Derechos[index]);
                    Derechos.splice(0,1);
                    CrearArbol(Node);
                }
             
              }

            }

    
      
}

function  CrearIzquierdo(Node)
{
       for (let i = 0; i < Izquierdos.length; i++) {
             if(Node.HijoI==null)
              {
                if(Node.Valor>Izquierdos[i])
                {
                    Node.HijoI=  new Nodo(Node,null,null,"Hijo Izquierdo",0,Izquierdos[i],Izquierdos[i]);
                    Izquierdos.splice(0,1);
                    CrearArbol(Node.HijoI);
                }
             
              }
    }
}