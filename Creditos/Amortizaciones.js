class Amortizaciones {

    constructor (periodo,inicio,fin,dias,disp,saldo,amortiza,interes,iva,flujo)
    {
        this.Periodo = periodo;
        this.Inicio = inicio;
        this.Fin = fin;
        this.Dias = dias;
        this.Disposicion = disp;
        this.Saldo = saldo;
        this.Amortizacion = amortiza;
        this.Interes = interes;
        this.Iva = iva;
        this.Flujo = flujo;
    }
}

var TablaAmotizacion=[];
var Meses=0;
function NuevoCredito(fechaInicio,Meses,Monto,Interes)
{
    var Inicio;
    var Fin;
    
   for (let index = 0; index < Meses; index++) {
      
    var amortizacion= new Amortizaciones();
    amortizacion.Periodo=index+1;
    if(TablaAmotizacion.length==0)
    { 
        var e = new Date(fechaInicio);
        amortizacion.Inicio=e.getFullYear() +"-"+ (e.getMonth()+1) +"-"+ e.getDate();
        e.setMonth(e.getMonth() + 1)

        amortizacion.Fin=e.getFullYear() +"-"+ (e.getMonth()+1) +"-"+ e.getDate();
        Inicio = moment(amortizacion.Inicio);
        Fin = moment(amortizacion.Fin);
        amortizacion.Dias=Fin.diff(Inicio, 'days');
        amortizacion.Saldo=Monto;
        amortizacion.Amortizacion=Monto/Meses;
        amortizacion.Interes=Monto*Interes/360*amortizacion.Dias;
        amortizacion.Iva=amortizacion.Interes*0.16;
        amortizacion.Flujo=amortizacion.Amortizacion+amortizacion.Interes+amortizacion.Iva;
        amortizacion.Disposicion=Monto;
         
    

    }else {
        var e = new Date(TablaAmotizacion[index-1].Inicio);
        var d = new Date(e);
        d.setMonth(d.getMonth() + 2)
         e.setMonth(e.getMonth() + 1)


        amortizacion.Inicio=e.getFullYear() +"-"+ (e.getMonth()+1) +"-"+ e.getDate();
        amortizacion.Fin=d.getFullYear() +"-"+ (d.getMonth()+1) +"-"+ d.getDate();
        Inicio = moment(amortizacion.Inicio);
        Fin = moment(amortizacion.Fin);
        amortizacion.Dias=Fin.diff(Inicio, 'days');
        amortizacion.Saldo=TablaAmotizacion[index-1].Saldo - TablaAmotizacion[index-1].Amortizacion;
        amortizacion.Amortizacion=TablaAmotizacion[index-1].Amortizacion;
        amortizacion.Interes=amortizacion.Saldo*Interes/360*amortizacion.Dias;
        amortizacion.Iva=amortizacion.Interes*0.16;
        amortizacion.Flujo=amortizacion.Amortizacion+amortizacion.Interes+amortizacion.Iva;
        amortizacion.Disposicion=0;

    }

    

    

    TablaAmotizacion.push(amortizacion);
       
   }


   
   

}


